<?php

use Illuminate\Support\Facades\Route;

// basic_routes

Route::get('/', function () {
    return view('index');
});

Route::namespace('Admin')->prefix('admin')->name('admin.')->group(function (){

    Route::resource('/user','UserController');
    Route::resource('/role','RoleController');

});

Route::get('/contact', function () {
    return view('contact');
});

Route::get('/company', function () {
    return view('company');
});

Route::get('/location', function () {
    return view('locations');
});

Route::get('/anhydrous', function () {
    return view('products.anhydrous');
});

Route::get('/aqueous', function () {
    return view('products.aqueous');
});

Route::get('/dashboard', function () {
    return view('order.dashboard');
});

// basic_routes_end

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Auth::routes(['verify' => true]);