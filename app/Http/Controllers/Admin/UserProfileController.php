<?php

namespace App\Http\Controllers\Admin;

use App\Day;
use App\Http\Controllers\Controller;
use App\Reasons;
use App\User;
use App\districts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Intervention\Image\ImageManagerStatic as Image;

class UserProfileController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:member-request',['only' => ['requestMemberShip']]);
        $this->middleware('auth');
        /*dog ads*/
        $this->dog_seller = 1;
        $this->dog_wants = 2;
        $this->dog_cross = 6;
        $this->dog_bording = 5;

        /*dog supplies*/
        $this->dog_food_treat = 8;
        $this->dog_vitamin_suppliment = 9;
        $this->dog_shampoo_grooming = 10;
        $this->dog_cages_carriers = 11;
        $this->dog_beds = 12;
        $this->dog_import_export = 7;

        /*dog accessories*/
        $this->dog_flea_tick = '';
        $this->dog_bowl_feeder = 13;
        $this->dog_collar_leash = 14;
        $this->dog_toy = 15;

        /*dog training*/
        $this->dog_traineer = 4;
        /*dog clinic*/
        $this->veterinary = 3;

        $this->last_id = 0; // <<<< LAST INSERT ID SAVE TO THIS VARIABLE
        $this->dontBoost = 5; // <<<< Don't Boost id assign TO THIS VARIABLE
        $this->middleware('auth');
    }

    //
    public function index(){

        $userid = Auth::user()->id;

        $dog_ads = DB::table('dog_ads')
            ->leftjoin('ad_types', 'dog_ads.ad_type_id','=','ad_types.id')
            ->leftJoin('dog_breeds','dog_ads.breed_id','=', 'dog_breeds.id')
            ->leftJoin('districts','dog_ads.district_id','=','districts.id')
            ->leftJoin('cities','dog_ads.city_id','=','cities.id')
            ->leftJoin('ad_img_approves','dog_ads.ad_id','=','ad_img_approves.ad_id')
            ->leftJoin('ad_approves','dog_ads.ad_id','=','ad_approves.ad_id')
            ->where('dog_ads.is_active','=',1)
            ->where('dog_ads.expire_state','=',0)
            ->where('dog_ads.created_by','=',$userid)
            ->groupBy('dog_ads.ad_id')
            ->select('dog_ads.*','ad_types.ad_type_name','districts.name_en as districtName','dog_breeds.breed_name','cities.name_en as cityName','ad_img_approves.img_name','ad_approves.created_at as approvedOn')->get();

        $dog_acce_ads = DB::table('dog_accessories_ads')
            ->leftjoin('ad_types', 'dog_accessories_ads.ad_type_id','=','ad_types.id')
            ->leftJoin('districts','dog_accessories_ads.district_id','=','districts.id')
            ->leftJoin('cities','dog_accessories_ads.city_id','=','cities.id')
            ->leftJoin('ad_img_approves','dog_accessories_ads.ad_id','=','ad_img_approves.ad_id')
            ->leftJoin('ad_approves','dog_accessories_ads.ad_id','=','ad_approves.ad_id')
            ->where('dog_accessories_ads.is_active','=',1)
            ->where('dog_accessories_ads.expire_state','=',0)
            ->where('dog_accessories_ads.created_by','=',$userid)
            ->groupBy('dog_accessories_ads.ad_id')
            ->select('dog_accessories_ads.*','ad_types.ad_type_name','districts.name_en as districtName','cities.name_en as cityName','ad_img_approves.img_name','ad_approves.created_at as approvedOn')->get();

        $dog_tran_ads = DB::table('dog_trainer_ads')
            ->leftjoin('ad_types', 'dog_trainer_ads.ad_type_id','=','ad_types.id')
            ->leftJoin('districts','dog_trainer_ads.district_id','=','districts.id')
            ->leftJoin('cities','dog_trainer_ads.city_id','=','cities.id')
            ->leftJoin('ad_img_approves','dog_trainer_ads.ad_id','=','ad_img_approves.ad_id')
            ->leftJoin('ad_approves','dog_trainer_ads.ad_id','=','ad_approves.ad_id')
            ->where('dog_trainer_ads.is_active','=',1)
            ->where('dog_trainer_ads.expire_state','=',0)
            ->where('dog_trainer_ads.created_by','=',$userid)
            ->groupBy('dog_trainer_ads.ad_id')
            ->select('dog_trainer_ads.*','ad_types.ad_type_name','districts.name_en as districtName','cities.name_en as cityName','ad_img_approves.img_name','ad_approves.created_at as approvedOn')->get();

        $dog_clinic_ads = DB::table('dog_clinic_ads')
            ->leftjoin('ad_types', 'dog_clinic_ads.ad_type_id','=','ad_types.id')
            ->leftJoin('districts','dog_clinic_ads.district_id','=','districts.id')
            ->leftJoin('cities','dog_clinic_ads.city_id','=','cities.id')
            ->leftJoin('ad_img_approves','dog_clinic_ads.ad_id','=','ad_img_approves.ad_id')
            ->leftJoin('ad_approves','dog_clinic_ads.ad_id','=','ad_approves.ad_id')
            ->where('dog_clinic_ads.is_active','=',1)
            ->where('dog_clinic_ads.expire_state','=',0)
            ->where('dog_clinic_ads.created_by','=',$userid)
            ->groupBy('dog_clinic_ads.ad_id')
            ->select('dog_clinic_ads.*','ad_types.ad_type_name','districts.name_en as districtName','cities.name_en as cityName','ad_img_approves.img_name','ad_approves.created_at as approvedOn')->get();

        $dog_supp_ads = DB::table('dog_supplies_ads')
            ->leftjoin('ad_types', 'dog_supplies_ads.ad_type_id','=','ad_types.id')
            ->leftJoin('districts','dog_supplies_ads.district_id','=','districts.id')
            ->leftJoin('cities','dog_supplies_ads.city_id','=','cities.id')
            ->leftJoin('ad_img_approves','dog_supplies_ads.ad_id','=','ad_img_approves.ad_id')
            ->leftJoin('ad_approves','dog_supplies_ads.ad_id','=','ad_approves.ad_id')
            ->where('dog_supplies_ads.is_active','=',1)
            ->where('dog_supplies_ads.expire_state','=',0)
            ->where('dog_supplies_ads.created_by','=',$userid)
            ->groupBy('dog_supplies_ads.ad_id')
            ->select('dog_supplies_ads.*','ad_types.ad_type_name','districts.name_en as districtName','cities.name_en as cityName','ad_img_approves.img_name','ad_approves.created_at as approvedOn')->get();

        $all_active_ads = $dog_ads->merge($dog_acce_ads)->merge($dog_tran_ads)->merge($dog_clinic_ads)->merge($dog_supp_ads);

        return view('admin.members.userprofile')->with('all_active_ads', $all_active_ads);
    }
    public function ad_edit($ad_id, $ad_type_id)
    {

        $ad_data = null;
        $ad_img = null;

        if ($ad_type_id == $this->dog_seller || $ad_type_id == $this->dog_wants  || $ad_type_id == $this->dog_cross || $ad_type_id == $this->dog_bording) {
            $ad_data = DB::table('dog_ads')
                ->select('dog_ads.*', 'districts.name_en as districtName', 'cities.name_en as cityName', 'dog_breeds.breed_name')
                ->leftJoin('districts', 'districts.id', '=', 'dog_ads.district_id')
                ->leftJoin('cities', 'cities.id', '=', 'dog_ads.city_id')
                ->leftJoin('dog_breeds', 'dog_breeds.id', '=', 'dog_ads.breed_id')
                ->where('dog_ads.ad_id', '=', $ad_id)
                ->get();

            $ad_img = DB::table('ad_img_approves')
                ->select('ad_img_approves.img_name')
                ->where('ad_img_approves.ad_id', '=', $ad_id)
                ->get();
        } else if ($ad_type_id == $this->dog_flea_tick || $ad_type_id == $this->dog_bowl_feeder  || $ad_type_id == $this->dog_collar_leash || $ad_type_id == $this->dog_toy) {
            $ad_data = DB::table('dog_accessories_ads')
                ->select('dog_accessories_ads.*', 'districts.name_en as districtName', 'cities.name_en as cityName')
                ->leftJoin('districts', 'districts.id', '=', 'dog_accessories_ads.district_id')
                ->leftJoin('cities', 'cities.id', '=', 'dog_accessories_ads.city_id')
                ->where('dog_accessories_ads.ad_id', '=', $ad_id)
                ->get();
            $ad_img = DB::table('ad_img_approves')
                ->select('ad_img_approves.img_name')
                ->where('ad_img_approves.ad_id', '=', $ad_id)
                ->get();
        }

        else if ($ad_type_id == $this->dog_traineer) {
            $ad_data = DB::table('dog_trainer_ads')
                ->select('dog_trainer_ads.*', 'districts.name_en as districtName', 'cities.name_en as cityName')
                ->leftJoin('districts', 'districts.id', '=', 'dog_trainer_ads.district_id')
                ->leftJoin('cities', 'cities.id', '=', 'dog_trainer_ads.city_id')
                ->where('dog_trainer_ads.ad_id', '=', $ad_id)
                ->get();
            $ad_img = DB::table('ad_img_approves')
                ->select('ad_img_approves.img_name')
                ->where('ad_img_approves.ad_id', '=', $ad_id)
                ->get();
        }

        else if ($ad_type_id == $this->veterinary) {
            $ad_data = DB::table('dog_clinic_ads')
                ->select('dog_clinic_ads.*', 'districts.name_en as districtName', 'cities.name_en as cityName')
                ->leftJoin('districts', 'districts.id', '=', 'dog_clinic_ads.district_id')
                ->leftJoin('cities', 'cities.id', '=', 'dog_clinic_ads.city_id')
                ->where('dog_clinic_ads.ad_id', '=', $ad_id)
                ->get();
            $ad_img = DB::table('ad_img_approves')
                ->select('ad_img_approves.img_name')
                ->where('ad_img_approves.ad_id', '=', $ad_id)
                ->get();
        }
        else if ($ad_type_id = $this->dog_import_export || $ad_type_id == $this->dog_food_treat || $ad_type_id == $this->dog_vitamin_suppliment  || $ad_type_id == $this->dog_shampoo_grooming || $ad_type_id == $this->dog_cages_carriers || $ad_type_id == $this->dog_beds) {
            $ad_data = DB::table('dog_supplies_ads')
                ->select('dog_supplies_ads.*', 'districts.name_en as districtName', 'cities.name_en as cityName')
                ->leftJoin('districts', 'districts.id', '=', 'dog_supplies_ads.district_id')
                ->leftJoin('cities', 'cities.id', '=', 'dog_supplies_ads.city_id')
                ->where('dog_supplies_ads.ad_id', '=', $ad_id)
                ->get();
            $ad_img = DB::table('ad_img_approves')
                ->select('ad_img_approves.img_name')
                ->where('ad_img_approves.ad_id', '=', $ad_id)
                ->get();
        }
        // dd($ad_data);
        $districts = districts::all();
        $dogBreed = DB::table('dog_breeds')->get();

        return view('admin.members.ad_edit_form', compact('districts'))
            ->with('dogBreed', $dogBreed)
            ->with('ad_data', $ad_data)
            ->with('ad_img', $ad_img);
    }

    public function update_ad(Request $request, $ad_id, $ad_type_id)
    {

        $negotiable     = $request->input('ng');
        $old_img = $request->input('image');
        $is_negotiable = 0;
        $images  = $request->file('picture');
        $adSerial = $request->input('adserial');
        // dd($adSerial);

        if(count($images)<=5){  }else{

            /* IF IMAGES ARE MORE THAN 05 DISPLAY ERROR */
            $imgmsg="Ops.! You can upload 05 Images Only";

            return redirect(url()->previous())->with('imgmsg',$imgmsg);
        }

        foreach ($images as $key => $file) {
            // Get FileName

            $filenameWithExt = $file->getClientOriginalName();
            //Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);

            //Get just extension
            $extension = strtolower($file->getClientOriginalExtension());
            $fileNameToStore = $this->generateRandom() . time() . $adSerial . '.' . $extension;


            $img = Image::make($file->getRealPath());
            $watermark = Image::make(public_path('/images/my_imgs/doglk_withlogo_watermark.png'));
//            $img->resize(800, 800);
            $img->insert($watermark, 'center');
            $img->save(public_path() . '/images/DogsADs/'.'/'.$fileNameToStore);

            $details = array(
                'ad_id' => $ad_id,
                'ad_type_id'=>$ad_type_id,
                'img_name' =>$fileNameToStore,
                'approved_by'=>auth()->user()->id,
            );
            DB::table('ad_update_img_approves')->insert($details);

        }


        if(isset($negotiable)){

            $is_negotiable = 1;
        }

        $checkExist = DB::table('ad_updates')->where('ad_id', $ad_id)->value('ad_id');

        if($checkExist == $ad_id)
        {
            DB::table('ad_updates')->where('ad_id', $ad_id)->delete();
        }


        $details = array(
            'full_name' => $request->name,
            'email' => $request->email,
            'ad_id'=>$ad_id,
            'ad_type_id'=>$ad_type_id,
            'contact_no' => $request->contact_1,
            'contact_no2' => $request->contact_2,
            'district_id' => $request->Districts,
            'city_id' => $request->city_id,
            'start_date_time'=>$request->publish_day,
            'end_date_time'=>$request->expire_day,
            'price' => $request->price,
            'negotiable' => $is_negotiable,
            'ad_title' => $request->title,
            'detail' => $request->details,
            'address' => $request->address,
            'created_by'=>Auth::user()->id,
            'created_at'=>date('Y-m-d H:i:s')
        );

        DB::table('ad_updates')->insert($details);

        if ($ad_type_id == $this->dog_seller || $ad_type_id == $this->dog_wants  || $ad_type_id == $this->dog_cross || $ad_type_id == $this->dog_bording) {

            DB::update('update dog_ads set is_active= ? where ad_id = ? ' ,[0,$ad_id]);


        } else if ($ad_type_id == $this->dog_flea_tick || $ad_type_id == $this->dog_bowl_feeder  || $ad_type_id == $this->dog_collar_leash || $ad_type_id == $this->dog_toy) {

            DB::update('update dog_accessories_ads set is_active= ? where ad_id = ? ' ,[0,$ad_id]);
        }

        else if ($ad_type_id == $this->dog_traineer) {
            DB::update('update dog_trainer_ads set is_active= ? where ad_id = ? ' ,[0,$ad_id]);

        }

        else if ($ad_type_id == $this->veterinary) {
            DB::update('update dog_clinic_ads set is_active= ? where ad_id = ? ' ,[0,$ad_id]);

        }
        else if ($ad_type_id = $this->dog_import_export || $ad_type_id == $this->dog_food_treat || $ad_type_id == $this->dog_vitamin_suppliment  || $ad_type_id == $this->dog_shampoo_grooming || $ad_type_id == $this->dog_cages_carriers || $ad_type_id == $this->dog_beds) {

            DB::update('update dog_supplies_ads set is_active= ? where ad_id = ? ' ,[0,$ad_id]);
        }


        return redirect(url()->previous());
    }
    public function generateRandom(){

        $random_id_length = 4;
        $rndid = password_hash("doglk0789654123webtude", PASSWORD_DEFAULT);
        $rndid = strip_tags(stripslashes($rndid));
        $rndid = str_replace(".","",$rndid);
        $rndid = strrev(str_replace("/","",$rndid));
        $rndid = substr($rndid,0,$random_id_length);

        return $rndid;
    }

    public function requestMemberShip(){

//        Permission::create(['name' => 'member area']);
        $role = Role::findById(1);
        $permission = Permission::findById(3);
//        $role->givePermissionTo($permission);
//        auth()->user()->givePermissionTo($permission);
//        auth()->user()->assignRole($role);
//        auth()->user()->getPermissionsViaRoles();
//        User::role('admin')->get();
        return auth()->user()->getPermissionsViaRoles();

        $days = Day::all();
        return view('admin.members.membership',compact('days'));
    }

    public function myProfile(){

        $deleteReasons = Reasons::where('reason_type','=','account_deactivate')->get();

        return view('admin.members.profilesettings',compact('deleteReasons'));
    }

    protected function updateMyProfile(Request $request)
    {
        try {
            $UpdateDetails = User::find(Auth::user()->id);
            $UpdateDetails->name = $request->name;

            if (request()->avatar != null) {
                $imageName = 'avatar' . time() . '.' . request()->avatar->getClientOriginalExtension();
                request()->avatar->move(public_path('/uploads/avatars'), $imageName);
                $UpdateDetails->avatar = $imageName;
            }
            $res = $UpdateDetails->save();
            if ($res)
                return redirect()->back()->with('success','Profile updated successfully!');
            else
                return redirect()->back()->with('error', $res);
        }catch(\Exception $e){

            $failmsg="Ops.! Something Wrong with Image Upload. Please Try Again";
            return redirect()->back()->with('error', $failmsg);
        }
    }

    public function changePassword(Request $request)
    {

        if (!(Hash::check($request->get('current-password'), Auth::user()->password))) {
            // The passwords matches
            return redirect()->back()->with("error", "Your current password does not matches with the password you provided. Please try again.");
        }

        if (strcmp($request->get('current-password'), $request->get('new-password')) == 0) {
            //Current password and new password are same
            return redirect()->back()->with("error", "New Password cannot be same as your current password. Please choose a different password.");
        }

        $validatedData = $request->validate([
            'current-password' => 'required',
            'new-password' => 'required|string|min:6|confirmed',
        ]);

        //Change Password
        $user = Auth::user();
        $user->password = Hash::make($request->get('new-password'));
        $user->save();

        return redirect()->back()->with("success", "Password changed successfully !");
    }

    public function favouriteAds(){

        return "under dev";
    }

    public function myMembership(){

        return "under dev mem";
    }

    public function inquiries(){

        $reasons = Reasons::where('reason_type','=','normal_inq')->get();

        return view('admin.members.usermessage',compact('reasons'));
    }

    public function sendInquiries(Request $request){

        if(!empty($request->title) && !empty($request->message)) {

            $inquery_no = uniqid('INQID-');
            $user_id = Auth::user()->id;
            $titles = $request->title;

            $title = '';
            foreach ($titles as $value) {
                $title .= $value . ',';
            }

            DB::table('user_inquiries')->insert(
                array(
                    'user_id' => $user_id,
                    'inquiry_no' => $inquery_no,
                    'msg' => $request->message,
                    'title' => $title,
                    'is_respond' => '0',
                    'created_at' => date('Y-m-d H:i:s'),
                )
            );
            return redirect()->back()->with('success',"We received your message.Your inquire ID is : $inquery_no. Our agent will join with you soon.");
        }
        elseif(empty($request->title) || empty($request->message)){
            return redirect()->back()->with('error', 'Required fields are must be filled!');
        }
    }
}
